import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import './Main.scss';
import Category from '../category/Category';
import Cart from '../cart/Cart';
import Product from '../product/Product';

const Main = (props) => {
    return <div className="main">
        <Route exact path="/" component={Category} />
        <Route path="/cart" component={Cart}/>
        <Route path="/product/:title" component={Product}/>
    </div>;
};
export default Main;
