import React, {
    Component,
    useEffect,
    useState
} from 'react';
import './Category.scss';
import CategoryItem from '../categoryItem/CategoryItem';
import productService from '../services/product.service';

const Category = (props) => {
    const [products, setProducts] = useState([]);

    useEffect(() => {
        productService.getProducts()
            .then((response) => {
                response.forEach((item) => {
                    item.image = `/media/${item.image}`;
                });
                setProducts(response);
            });
    }, []);

    return (
        <div className="category">
            <div className="category__header">
                <img src="/media/plates-header.jpg"/>
                <div className="header__text">Plates</div>
            </div>
            <div className="category__items">
                {products.map((item) => {
                    return (<CategoryItem key={item.title} item={item}></CategoryItem>);
                })}
            </div>
        </div>
    );
};

export default Category;
