import React, { useState, useContext, useRef } from 'react';
import { Link } from 'react-router-dom';
import ModalAnchor from '../modalAnchor/ModalAnchor';
import CartPopup from '../cartPopup/CartPopup';
import './Header.scss';
import {CartContext} from '../contexts/CartContext';

const Header = (props) => {
    const myCartElement = useRef(null);
    const cartContext = useContext(CartContext);
    const [count, setCount] = useState(cartContext.getTotalCount());
    const [totalPrice, setTotalPrice] = useState(cartContext.getTotalPrice());
    const [isShowModal, setIsShowModal] = useState(false);
    const setCountValueInHeader = (count, price) => {
        setCount(count);
        setTotalPrice(price);
    };
    const switchModal = () => {
        setIsShowModal(!isShowModal);
    };
    cartContext.setCountValueInHeader = setCountValueInHeader;

    return <div className="header">
        <div className="header__logo"></div>
        <div className="header__links">
            <Link to="/" className="links__button">HOME</Link>
            <Link to="/" className="links__button">SHOP</Link>
            <Link to="/" className="links__button">JOURNAL</Link>
            <Link to="/" className="links__button">MORE</Link>
        </div>
        <div ref={myCartElement} className="header__cart" onClick={switchModal}>MY CART{` (${count})`}</div>
        {
            isShowModal
                ? <ModalAnchor targetElement={myCartElement.current} onOverlayClick={switchModal} direction="bottom" align="left">
                    <CartPopup totalPrice={totalPrice}/>
                </ModalAnchor>
                : null
        }
    </div>;
};
export default Header;
