import React, { Fragment } from 'react';
import Header from './header/Header';
import Main from './main/Main';
import {CartContext, cart} from './contexts/CartContext';
import './App.scss';

const App = () => {
    return <CartContext.Provider value={cart}>
            <Fragment>
                <Header></Header>
                <Main></Main>
            </Fragment>
        </CartContext.Provider>;
};

export default App;
