import React, {useState, useRef} from 'react';
import ModalAnchor from '../modalAnchor/ModalAnchor';
import ProductInfo from '../productInfo/ProductInfo';
import './CategoryItem.scss';
import PropTypes from 'prop-types';

const CategoryItem = (props) => {
    const itemElement = useRef();
    const item = props.item;
    const [isShowModal, setShowModal] = useState(false);

    const handleMouseEnter = () => {
        setShowModal(true);
    };

    const handleMouseLeave = () => {
        setShowModal(false);
    };

    return (
        <div
            className="category-item"
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
        >
            {isShowModal
                ? <ProductInfo item={item} />
                : <div ref={itemElement} className="category-item__image">
                    <img src={item.image}/>
                </div>}
            <div className="category-item__brand">{item.brand}</div>
            <div className="category-item__title">{item.title.toUpperCase()}</div>
            <div className="category-item__price">{item.price + '$'}</div>
        </div>
    );
};

CategoryItem.propTypes = {
    item: PropTypes.shape({
        image: PropTypes.string,
        brand: PropTypes.string,
        title: PropTypes.string,
        price: PropTypes.number
    })
};

export default CategoryItem;
