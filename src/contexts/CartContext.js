import React from 'react';

class Cart {
    constructor() {
        this.items = new Map();
        this.increaseCount = this.increaseCount.bind(this);
        this.decreaseCount = this.decreaseCount.bind(this);
        this.removeItem = this.removeItem.bind(this);
    }

    addItem(key, item, count) {
        const value = this.items.get(key);
        if (value) {
            value.count++;
        } else {
            this.items.set(key, {
                item,
                count
            });
        }
        this.setCountValueInHeader(this.getTotalCount(), this.getTotalPrice());
    }

    getTotalPrice() {
        let price = 0;
        this.items.forEach((value) => {
            price += value.item.price * value.count;
        });
        return price;
    }

    getTotalCount() {
        let count = 0;
        this.items.forEach((value) => {
            count += value.count;
        });
        return count;
    }

    removeItem(key) {
        this.items.delete(key);
        this.setCountValueInHeader(this.getTotalCount(), this.getTotalPrice());
    }

    increaseCount (key){
        const value = this.items.get(key);
        value.count += 1;
        this.setCountValueInHeader(this.getTotalCount(), this.getTotalPrice());
    }

    decreaseCount (key) {
        const value = this.items.get(key);
        if (value.count > 0) {
            value.count -= 1;
            this.setCountValueInHeader(this.getTotalCount(), this.getTotalPrice());
        }
    }

    setCountValueInHeader() {

    }
}

export const cart = new Cart();

export const CartContext = React.createContext();