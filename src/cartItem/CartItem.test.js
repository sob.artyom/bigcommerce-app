import React from "react";
import { mount } from "enzyme";
import sinon from 'sinon';
import CartItem from "./CartItem";
import 'jest'

let item;
let systemUnderTest;
let increaseCountStub;
let decreaseCountStub;

describe("CartItem", () => {
    test("renders", () => {
        item = {
            item: {
                title: 'titleName1'
            },
            count: 0
        };
        systemUnderTest = mount(<CartItem
            item={item}
        />);
        expect(systemUnderTest).toBeDefined();
    });

    describe('element with class .item-controls', () => {
        item = {
            item: {
                title: 'titleName2'
            },
            count: 0
        };
        increaseCountStub = sinon.spy();
        decreaseCountStub = sinon.spy();

        systemUnderTest = mount(<CartItem
            item={item}
            increaseCount={increaseCountStub}
            decreaseCount={decreaseCountStub}
        />);
        const itemControls = systemUnderTest.find('.item-controls');
        const button = itemControls.find('.controls-button').first();

        test('should be defined', () => {
            expect(itemControls).toBeDefined();
        });

        test('should call increaseCount function', () => {
            expect(increaseCountStub.calledOnce).toEqual(false);
            button.simulate('click');
            expect(increaseCountStub.calledOnce).toEqual(true);
        });
    });
});