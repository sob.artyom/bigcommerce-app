import React from 'react';
import PropTypes from 'prop-types';
import './CartItem.scss';

const CartItem = (props) => {
    const {
        item,
        increaseCount,
        decreaseCount,
        removeItem
    } = props;

    const handlePlusButtonClick = () => {
        increaseCount(item.item.title);
    };
    const handleMinusButtonClick = () => {
        decreaseCount(item.item.title);
    };
    const handleRemoveButtonClick = () => {
        removeItem(item.item.title);
    };

    return <div className="cart-item">
        <div>{`title: ${item.item.title}`}</div>
        <div>{`price: ${item.item.price}$`}</div>
        <div>{`count: ${item.count}`}</div>
        <div className="item-controls">
            <div className="controls-button" onClick={handlePlusButtonClick}>+</div>
            <div className="controls-button" onClick={handleMinusButtonClick}>-</div>
            <div className="controls-button" onClick={handleRemoveButtonClick}>remove</div>
        </div>
    </div>;
};

CartItem.propTypes = {
    item: PropTypes.object,
    increaseCount: PropTypes.func,
    decreaseCount: PropTypes.func,
    removeItem: PropTypes.func,
};

export default CartItem;