import React, {useContext} from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import './ProductInfo.scss';
import {CartContext} from '../contexts/CartContext';

const ProductInfo = (props) => {
    const cartContext = useContext(CartContext);
    const {item} = props;
    const path = `/product/${item.title}`;
    const handleClick = () => {
        cartContext.addItem(item.title, item, 1);
    };

    return (<div className="product-info">
        <div className="product-info__buttons">
            <Link className="buttons__button" to={path}>
                VIEW DETAILS
            </Link>
            <div className="buttons__button" onClick={handleClick}>
                ADD TO CARD
            </div>
        </div>
    </div>);
};

ProductInfo.propTypes = {
    item: PropTypes.shape({
        image: PropTypes.string,
        brand: PropTypes.string,
        title: PropTypes.string,
        price: PropTypes.number
    })
};

export default ProductInfo;