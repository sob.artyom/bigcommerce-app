/**
 * Created by soart on 11/6/2019.
 */
import axios from 'axios';

async function getProducts () {
    let result = [];
    try {
        const response = await axios.get('/products.json');
        result = response.data;
    } catch (error) {
        console.log(error);
    }
    return result;
}

export default {
    getProducts
};