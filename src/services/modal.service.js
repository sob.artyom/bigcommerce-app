function getModalPosition(align, direction, targetElement) {
    const result = {};
    const { clientHeight, clientWidth } = document.body;
    const {offsetTop, offsetLeft} = targetElement;
    const {width, height} = targetElement.getBoundingClientRect();
    switch (align) {
        case 'right':
            result['left'] = offsetLeft;
            break;
        default:
            result['right'] = clientWidth - offsetLeft - width;
            break;
    }

    switch (direction) {
        case 'bottom':
            result['top'] = offsetTop + height;
            break;
        default:
            result['bottom'] =  clientHeight - offsetTop - height;
            break;
    }

    return result;
}

export default {
    getModalPosition
};
