import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './ModalAnchor.scss';
import PropTypes from 'prop-types';
import modalService from '../services/modal.service';

const modalRoot = document.getElementById('modal-root');

const ModalAnchor = (props) => {
    const {
        targetElement,
        align,
        direction,
        onOverlayClick
    } = props;
    const positionStyles = modalService.getModalPosition(align, direction, targetElement);
    const [isRefresh, isSetRefresh] = useState(false);
    const [isRendered, setIsRendered] = useState(false);
    const handleOverlayClick = () => {
        onOverlayClick();
    };
    const wrapperElement = (
        <div
            className={isRendered ? 'modal modal--show' : ' modal modal--hide'}
            style={positionStyles}
        >
            <div className="overlay" onClick={handleOverlayClick}></div>
            <div className="modal__context">{props.children}</div>
        </div>
    );
    const forceRefresh = () => {
        isSetRefresh(!isRefresh);
    };

    useEffect(() => {
        setIsRendered(true);
        document.addEventListener('resize', forceRefresh);
        document.addEventListener('scroll', forceRefresh);
        return () => {
            document.removeEventListener('resize', forceRefresh);
            document.removeEventListener('scroll', forceRefresh);
        };
    }, []);

    return ReactDOM.createPortal(
        wrapperElement,
        modalRoot
    );
};

ModalAnchor.propTypes = {
    targetElement: PropTypes.object,
    align: PropTypes.string,
    direction: PropTypes.string,
    onOverlayClick: PropTypes.func
};

export default ModalAnchor;