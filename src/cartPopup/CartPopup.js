import React, { useContext } from 'react';
import CartItem from '../cartItem/CartItem';
import PropTypes from 'prop-types';
import {CartContext} from '../contexts/CartContext';
import './CartPopup.scss';

const CartPopup = (props) => {
    const {totalPrice} = props;
    const cartContext = useContext(CartContext);
    const { items, increaseCount, decreaseCount, removeItem } = cartContext;
    const cartItems = [];

    items.forEach((item, key) => {
        cartItems.push(<CartItem
            key={key}
            item={item}
            increaseCount={increaseCount}
            decreaseCount={decreaseCount}
            removeItem={removeItem}
        />);
    });

    return (
        <div className="cart-popup">
            {
                cartItems
            }
            <div>{`Total Price: ${totalPrice}`}</div>
        </div>
    );
};

CartPopup.propTypes = {
    totalPrice: PropTypes.number
};

export default CartPopup;