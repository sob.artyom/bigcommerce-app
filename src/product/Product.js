import React from 'react';

const Product = (props) => {
    const title = props.match.params.title;
    return (
        <div className="Product">
            Product: {title}
        </div>
    );
}

export default Product;
